from django.contrib import admin
from django.urls import path, include
from firstapp import views


urlpatterns = [
    path('posts/', include('firstapp.urls')),
    path('admin/', admin.site.urls),
]
