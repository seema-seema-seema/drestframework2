from rest_framework import serializers
from .models import Category, Post


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        # likhna jrori becuz error a rhi k eh version deprecate ho gya eh line likho ta likya
        fields = '__all__'

        # eda v kr sakde jive model form ch krde hi apa
        #fields = ['id', 'title','created_at']


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = '__all__'

        #fields = ['id', 'title']
